// Pruebas Unitarias
import { shallowMount } from '@vue/test-utils';
import Counter from '@/components/Counter';

describe('Counter Components', () => {
    test('h2 debe tener el valor por defecto Counter', () => {
        const wrapper = shallowMount(Counter);
        const h2 = wrapper.find('h2');
        const palabraEsperada = h2.text();
        expect(palabraEsperada).toBe('App que eleva al cuadrado un número')
    });
})


describe('Counter Components', () => {
    test('el valor por defecto debe ser 100 en el parrafo', () => {
        const wrapper = shallowMount(Counter);
        const p2 = wrapper.findAll('p');
        const palabraEsperada = p2;
        console.log(palabraEsperada);
        expect(palabraEsperada).toBe('2');
    });
})
